import cMath.funcs.FuncEval;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by idib on 30.03.17.
 */
public class lab9 {
    static Scanner in = new Scanner(System.in);
    static float MaxRand = (float) 0.02;
    static Random r = new Random();
    private static boolean rando = false;
//    private static boolean rando = true;

    private static Expression enterFX(String promt) {
        while (true) {
            try {
                System.out.println(promt);
                return new ExpressionBuilder(in.nextLine()).variable("x").build();

            } catch (Exception es) {
                System.out.println("incorrect");
            }
        }
    }

    public static void main(String[] args) throws Exception {
        FuncEval fx, ffx, fsx, ftx;
        fx = new FuncEval("f(x)=", enterFX("f(x)="));
        System.out.println("enter start x");
        double startx = in.nextDouble();
        System.out.println("enter end x");
        double endx = in.nextDouble();
        double step = 1e-6f;

        double s;
        System.out.println("\n\n\n");
        s = fx.integrateTrapezoid(startx, endx, step);
        System.out.println("Trapezoid fix step = " + step);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

        System.out.println("\n\n\n");
        s = fx.integrateRectangle(startx, endx, step);
        System.out.println("Rectangle (rigth) fix step = " + step);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

        System.out.println("\n\n\n");
        s = fx.integrateSimpson(startx, endx, step);
        System.out.println("Simpson fix step = " + step);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

        System.out.println("\n\n\n");
        s = fx.integrateTrapezoid(startx, endx);
        System.out.println("Trapezoid auto eps = " + fx.eps);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("step " +  fx.step);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

        System.out.println("\n\n\n");
        s = fx.integrateRectangle(startx, endx);
        System.out.println("Rectangle (rigth) auto eps = " + fx.eps);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("step " +  fx.step);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

        System.out.println("\n\n\n");
        s = fx.integrateSimpson(startx, endx);
        System.out.println("Simpson auto eps = " + fx.eps);
        System.out.println("count Itration");
        System.out.println(fx.countIter);
        System.out.println("step " +  fx.step);
        System.out.println("integal from " + startx + " to " + endx + " = " + s);

    }
}