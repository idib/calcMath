package cMath;

import cMath.funcs.Func;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.function.Function2D;
import org.jfree.data.function.NormalDistributionFunction2D;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import java.util.List;

/**
 * This demo shows a normal distribution function.
 */
public class chart extends ApplicationFrame {
    int w = 600, h = 600;

    public chart(final String title, List<Func> funcs, double start, double finish) {
        super(title);
        init(createDataset(funcs, start, finish));
    }

    public chart(final String title, List<Func> funcs, double start, double finish, int r) {
        super(title);
        init2(createDataset(funcs, start, finish));
    }

    public static void srun(String promt, List<Func> funcs, double start, double finish) {
        final chart demo = new chart(promt, funcs, start, finish);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    public static void srun2(String promt, List<Func> funcs, double start, double finish) {
        final chart demo = new chart(promt, funcs, start, finish,2);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    private XYDataset createDataset(List<Func> funcs, double start, double finish) {
        XYSeriesCollection result = new XYSeriesCollection();
        for (int j = 0; j < funcs.size(); j++) {
            result.addSeries(funcs.get(j).getPlot(start, finish));
        }
        return result;
    }

    private void init(XYDataset DSet) {
        Function2D normal = new NormalDistributionFunction2D(0.0, 1.0);
        final JFreeChart chart = ChartFactory.createXYLineChart(
                "",
                "x",
                "y",
                DSet,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(w, h));
        setContentPane(chartPanel);
    }

    private void init2(XYDataset var0) {
        JFreeChart var1 = ChartFactory.createXYLineChart("", "X", "Y", var0, PlotOrientation.VERTICAL, true, true, false);
        XYPlot var2 = (XYPlot)var1.getPlot();
        XYLineAndShapeRenderer var3 = new XYLineAndShapeRenderer();
        var3.setSeriesLinesVisible(1, false);
        var3.setSeriesShapesVisible(0, false);
        var3.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
        var2.setRenderer(var3);
        NumberAxis var4 = (NumberAxis)var2.getRangeAxis();
        var4.setStandardTickUnits(NumberAxis.createIntegerTickUnits());


        final ChartPanel chartPanel = new ChartPanel(var1);
        chartPanel.setPreferredSize(new java.awt.Dimension(w, h));
        setContentPane(chartPanel);
    }
}