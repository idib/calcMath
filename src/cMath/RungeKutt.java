package cMath;

import cMath.funcs.FuncOnPoint;
import javafx.geometry.Point2D;

/**
 * Created by idib on 28.04.17.
 */
public abstract class RungeKutt {
    protected int size;
    private double[] k1, k2, k3, k4;

    public int Meth_Auto(FuncOnPoint[] funcs, double start, double finish, double eps) {
        int i = 1;
        double len = finish - start;
        do {
            double[] y = Step(start, len, getLast(funcs));

            double err = Double.NEGATIVE_INFINITY;
            for (int j = 0; j < size; j++)
                err = Math.max(err, Math.abs(2 * (k1[j] - k2[j] - k3[j] + k4[j]) / 3));

            if (eps >= err) {
                addLast(funcs, y, start);
                start += len;
                len = finish - start;
                i++;
            } else
                len = len / 2;
        } while (start < finish);
        return i;
    }

    public void Meth_Const(FuncOnPoint[] funcs, double start, double finish, double count) {
        double[] y;
        double step = (finish - start) / count;
        for (int i = 0; i < count; i++)
            addLast(funcs, Step(start + i * step, step, getLast(funcs)), start + i * step);
    }

    protected abstract FuncOnPoint[] Solution(FuncOnPoint[] funcs) ;

    private double[] Step(double t, double step, double[] Y) {
        double[] resault = new double[size];
        double[] temp_Y = new double[size];

        k1 = SystemFunc(t, Y);
        for (int i = 0; i < size; i++)
            temp_Y[i] = Y[i] + k1[i] * (step / 2.0);

        k2 = SystemFunc(t + step / 2.0, temp_Y);
        for (int i = 0; i < size; i++)
            temp_Y[i] = Y[i] + k2[i] * (step / 2.0);

        k3 = SystemFunc(t + step / 2.0, temp_Y);
        for (int i = 0; i < size; i++)
            temp_Y[i] = Y[i] + k3[i] * step;

        k4 = SystemFunc(t + step, temp_Y);

        for (int i = 0; i < size; i++)
            resault[i] = Y[i] + step / 6.0 * (k1[i] + 2.0 * k2[i] + 2.0 * k3[i] + k4[i]);

        return resault;
    }

    protected abstract double[] SystemFunc(double x, double[] y);

    protected abstract void YStart(FuncOnPoint[] f);

    private void addLast(FuncOnPoint[] funcs, double[] y, double x) {
        for (int i = 0; i < funcs.length; i++)
            funcs[i].add(new Point2D(x, y[i]));
    }

    private double[] getLast(FuncOnPoint[] funcs) {
        double[] res = new double[funcs.length];
        for (int i = 0; i < funcs.length; i++)
            res[i] = funcs[i].get(funcs[i].size() - 1).getY();
        return res;
    }
}
