import cMath.Matrix;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Alexandr on 20.04.2017.
 */
public class lab12 {
    private static double eps = 0.0001;
    private static ScriptEngineManager manager = new ScriptEngineManager();
    private static ScriptEngine engine = manager.getEngineByName("js");
    private static double[] X;
    /*private static String[] system = {"x0^5-x1", "x0^2+x1^2-25"};
    private static String[] _system = {"Math.pow(x0,5)-x1", "Math.pow(x0,2)+Math.pow(x1,2)-25"};
    private static String[][] jacobi = {
            {"5*Math.pow(x0,4)", "2*x0"},
            {"-1", "2*x1"}
    };*/
    private static String[] system = {"6*x0^3+2*x1^2+x2+4", "2*x1^3+5*x0^2+7" , "32*x2^3+x0+11"};
    private static String[] _system = {"6*Math.pow(x0,3)+2*Math.pow(x1,2)+x2+4", "2*Math.pow(x1,3)+5*Math.pow(x0,2)+7" , "32*Math.pow(x2,3)+x0+11"};
    private static String[][] jacobi = {
        {"18*Math.pow(x0,2)", "10*x0" , "1"},
        {"4*x1", "6*Math.pow(x1,2)" , "0"},
        {"1", "0" , "96*Math.pow(x2,2)"},
    };

    private static double[] Newtone_Method(){
        int n = system.length;
        double[] x1;
        double[] x2 = X.clone();
        double sum = 1;
        do{
            sum = 0;
            x1 = x2.clone();
            double[] dx = delta(x1);
            for (int i = 0; i < n; i++){
                x2[i]-=dx[i];
                sum += Math.abs(x1[i]-x2[i]);
            }
        }while(sum > eps);
        return x1;
    }

    private static double[] delta(double[] x1){
        int n = system.length;
        double b[][] = new double[1][n];
        double a[][] = new double[n][n];
        for (int i = 0; i < n; i++){
            String cur = _system[i];
            for (int j = 0; j < n; j++)
                cur = cur.replaceAll("x"+j,x1[j]+"");

            try {
                b[0][i] = (Double)engine.eval(cur);
            }
            catch (Exception e){

            }

            for (int j = 0; j < n; j++){
                cur = jacobi[i][j];
                for (int k = 0; k < n; k++)
                    cur = cur.replaceAll("x"+k,x1[k]+"");
                try {
                    Object res = engine.eval(cur);
                    if (res instanceof Integer)
                        a[i][j] = (Integer)res*1.0;
                    else{
                        a[i][j] = (Double)res;
                    }
                }
                catch (Exception e){

                }
            }

        }

        Matrix A = new Matrix(a);
        Matrix B = new Matrix(b);

        Matrix X;
        try {
            double[][] curarr = inverz_matrike(A.getMat());
            X = new Matrix(curarr);
            X = Matrix.mul(B,X);
            return X.getMat()[0];
        }
        catch (Exception e){

        }
        return new double[n];
    }

    public static double[][] inverz_matrike(double[][]in){
        int st_vrs=in.length, st_stolp=in[0].length;
        double[][]out=new double[st_vrs][st_stolp];
        double[][]old=new double[st_vrs][st_stolp*2];
		double[][]_new=new double[st_vrs][st_stolp*2];


        for (int v=0;v<st_vrs;v++){//ones vector
            for (int s=0;s<st_stolp*2;s++){
                if (s-v==st_vrs)
                    old[v][s]=1;
                if(s<st_stolp)
                    old[v][s]=in[v][s];
            }
        }
        //zeros below the diagonal
        for (int v=0;v<st_vrs;v++){
            for (int v1=0;v1<st_vrs;v1++){
                for (int s=0;s<st_stolp*2;s++){
                    if (v==v1)
                        _new[v][s]=old[v][s]/old[v][v];
                    else
                        _new[v1][s]=old[v1][s];
                }
            }
            old=prepisi(_new);
            for (int v1=v+1;v1<st_vrs;v1++){
                for (int s=0;s<st_stolp*2;s++){
                    _new[v1][s]=old[v1][s]-old[v][s]*old[v1][v];
                }
            }
            old=prepisi(_new);
        }
        //zeros above the diagonal
        for (int s=st_stolp-1;s>0;s--){
            for (int v=s-1;v>=0;v--){
                for (int s1=0;s1<st_stolp*2;s1++){
                    _new[v][s1]=old[v][s1]-old[s][s1]*old[v][s];
                }
            }
            old=prepisi(_new);
        }
        for (int v=0;v<st_vrs;v++){//rigt part of matrix is invers
            for (int s=st_stolp;s<st_stolp*2;s++){
                out[v][s-st_stolp]=_new[v][s];
            }
        }
        return out;
    }

    public static double[][] prepisi(double[][]in){
        double[][]out=new double[in.length][in[0].length];
        for(int v=0;v<in.length;v++){
            for (int s=0;s<in[0].length;s++){
                out[v][s]=in[v][s];
            }
        }
        return out;
    }

    public static void main(String[] args) throws Exception{
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));
        int n = system.length;
        X = new double[n];
        Arrays.fill(X,1.0);

        for (int i=0; i<n; i++){
            out.write("f"+i+"(X)="+system[i]+"\n");
        }
        double[] ans = Newtone_Method();

        out.write("\n-----------------------------------\n\n");

        for (int i=0; i<n; i++) {
            out.write("x"+i+"="+ans[i]+"\n");
        }

        out.write("\n-----------------------------------\n\n");

        String cur;
        double tmp = 0;
        for (int i = 0; i < n; i++){
            cur = _system[i];
            for (int k = 0; k < n; k++)
                cur = cur.replaceAll("x"+k,ans[k]+"");
            try {
                Object res = engine.eval(cur);
                if (res instanceof Integer)
                    tmp = (Integer)res*1.0;
                else{
                    tmp = (Double)res;
                }
            }
            catch (Exception e){

            }
            out.write("f"+i+"(X)="+tmp+"\n");
        }

        out.close();
    }
}
