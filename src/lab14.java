import cMath.RungeKutt;
import cMath.chart;
import cMath.funcs.FuncOnPoint;
import javafx.geometry.Point2D;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by idib on 28.04.17.
 */
public class lab14 {
    static int size = 2;
    private static double EPS = 0.000001;
    private static final double ASTEP = 1;

    public static void main(String[] args) throws IOException {
        sys s = new sys();



        double l = 0, lv = s.calcWith(l), r = 0, rv;
        do
            rv = s.calcWith(r += ASTEP);
        while (lv > 0 == rv > 0);
        while (Math.abs(rv) > EPS) {
            double c = (l + r) / 2, cv = s.calcWith(c);
            if (lv > 0 != cv > 0) {
                r = c;
                rv = cv;
            } else {
                l = c;
                lv = cv;
            }
        }

        s.calcWith(r, 10, true);

    }

    static class sys extends RungeKutt {
        public sys() {
            size = 2;
        }

        @Override
        protected FuncOnPoint[] Solution(FuncOnPoint[] funcs) {
            //double[] y = new double[size];
            FuncOnPoint[] res = new FuncOnPoint[funcs.length];
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
            return res;
        }

        @Override
        protected double[] SystemFunc(double x, double[] y) {
//        return new double[]{
//                -2*y[0]+4*y[1],
//                -y[0]+3*y[1]};
            return new double[]{
                    y[1],
                    -0.4*Math.cos(y[0])};
        }

        @Override
        protected void YStart(FuncOnPoint[] f) {
            f[0].add(new Point2D(0, 1));
            f[1].add(new Point2D(0, 1));

        }

        public double calcWith(double a, double r, boolean vis) {
            FuncOnPoint[] Y2 = new FuncOnPoint[size];
            for (int i = 0; i < Y2.length; i++)
                Y2[i] = new FuncOnPoint("y" + i);

            Y2[0].add(new Point2D(0, 0));
            Y2[1].add(new Point2D(0, a));

            if (!vis)
                Meth_Auto(Y2, 0, r, EPS);
            else
                Meth_Const(Y2,0,r, 10000);
            if (vis)
                chart.srun("", new ArrayList<>(Arrays.asList(Y2)), 0, r);

            Point2D i = Y2[1].get(0);
            for (Point2D point2D : Y2[1]) {
                if (Math.abs(i.getX() - 1) < Math.abs(point2D.getX() - 1))
                    i = point2D;
            }

            System.out.println("u'(0) = " + a + ",  u'(1) = " + i.getY());
            return i.getY();
        }

        public double calcWith(double a) {
            return calcWith(a, 1, false);
        }
    }
}
