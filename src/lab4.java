import cMath.Matrix;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by idib on 08.02.17.
 */
public class lab4 {
    public static void main(String[] args) throws Exception {
        double eps = (double) 1e-6;
        NumberFormat formater = new DecimalFormat("0.#####E0##");

        Matrix A, X;

        // test1
        A = new Matrix("src/tests/matrix/matrix_lambda_a_1");
        X = new Matrix("src/tests/matrix/matrix_lambda_x_1");
//
//        test2
//        A = new Matrix("src/tests/matrix/matrix_lambda_a_1");
        X = new Matrix("src/tests/matrix/matrix_lambda_x_2");

        //test3
//        A = new Matrix("src/tests/matrix/matrix_a_7");
//        B = new Matrix("src/tests/matrix/matrix_b_7");

        System.out.println("eps");
        System.out.println(formater.format(eps));
        System.out.println();

        System.out.println("A=");
        System.out.println(A.toString());
        System.out.println();

        System.out.println("X_start=");
        System.out.println(X.toString());
        System.out.println();

        try {
            System.out.println("count iterations:");

            Matrix XB_max = A.LambdaWithVector(X,eps);
            Matrix XB_min = A.FindMinLambda(X,eps);
            Matrix XB_two = A.findTwoLambda(X,eps);
            System.out.println(XB_max.countIterations);
            System.out.println();
            System.out.println("X_for_lambda=");
            System.out.println(XB_max);
            System.out.println(XB_min);
            System.out.println(XB_two);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
