import cMath.Matrix;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Created by idib on 22.03.17.
 */
public class lab5 {
    public static void main(String[] args) throws Exception {
        double eps = (double) 1e-9;
        NumberFormat formater = new DecimalFormat("0.#####E0##");

        Matrix A;

        // test1
        A = new Matrix("src/tests/matrix/matrix_lambda_a_1");

        System.out.println("eps");
        System.out.println(formater.format(eps));
        System.out.println();

        System.out.println("A=");
        System.out.println(A.toString());
        System.out.println();

        try {
            List<Matrix> res = A.Lambda(eps);
            System.out.println("count iterations:");
            System.out.println(A.countIterations);
            System.out.println();
//            System.out.println(res);
            for (Matrix re : res)
                System.out.println(re);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}