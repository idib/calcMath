package lab10;

import cMath.funcs.CubicSpline;
import cMath.funcs.Func;
import cMath.funcs.FuncCombine;
import cMath.funcs.FuncOnPoint;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Controller {
    @FXML
    public Label Sim;
    @FXML
    public TextField CountPText;
    @FXML
    public Label Mon;
    @FXML
    Pane pane;
    @FXML
    Canvas canvas;
    Random r = new Random();
    private FuncOnPoint polygon;
    private GraphicsContext draw;

    private void DrPoly(FuncOnPoint f, GraphicsContext g_c, double w, double h) {
        draw.setStroke(Color.BLACK);
        double Xlen = f.maxX() - f.minX();
        double Ylen = f.maxY() - f.minY();
        double scaleX = canvas.getWidth() / (Xlen);
        double scaleY = canvas.getHeight() / (Ylen);
        double xo = -f.minX() * scaleX + w / 2 - Xlen * scaleX / 2, yo = f.minY() * scaleY + h / 2 + Ylen * scaleY / 2;
        for (int i = 0; i < f.size() - 1; i++) {
            g_c.strokeLine(xo + f.get(i).getX() * scaleX, yo - f.get(i).getY() * scaleY, xo + f.get(i + 1).getX() * scaleX, yo - f.get(i + 1).getY() * scaleY);
        }
        g_c.strokeLine(xo + f.get(f.size() - 1).getX() * scaleX, yo - f.get(f.size() - 1).getY() * scaleY, xo + f.get(0).getX() * scaleX, yo - f.get(0).getY() * scaleY);
    }

    private double Monte(FuncOnPoint f, int n, GraphicsContext g_c) {
        int good = 0;
        double fmaxx = f.maxX(), fminx = f.minX(), fmaxy = f.maxY(), fminy = f.minY();
        double Xlen = fmaxx - fminx;
        double Ylen = fmaxy - fminy;
        double scaleX = canvas.getWidth() / (Xlen);
        double scaleY = canvas.getHeight() / (Ylen);
        double xo = -fminx * scaleX + canvas.getWidth() / 2 - Xlen * scaleX / 2, yo = fminy * scaleY + canvas.getHeight() / 2 + Ylen * scaleY / 2;
        for (int i = 0; i < n; i++) {
            double x = r.nextDouble() * Xlen + fminx;
            double y = r.nextDouble() * Ylen + fminy;
            draw.setStroke(Color.BLUE);
            if (f.pnpoly(new Point2D(x, y))) {
                draw.setStroke(Color.RED);
                good++;
            }
            drPoint(new Point2D(x, y), g_c, xo, yo, scaleX, scaleY);
        }
        Xlen = fmaxx - fminx;
        Ylen = fmaxy - fminy;
        return good * Xlen * Ylen / n;
    }

    private void drPoint(Point2D p, GraphicsContext g_c, double xo, double yo, double scaleX, double scaleY) {
        draw.setLineWidth(2);
        draw.strokeLine(xo + p.getX() * scaleX, yo - p.getY() * scaleY, xo + p.getX() * scaleX, yo - p.getY() * scaleY);
    }

    @FXML
    public void initialize() throws FileNotFoundException {
        Func xt, yt;
        Scanner in;

        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();
        List<Double> t = new ArrayList<>();

        // test1
//        in = new Scanner(new File("src/tests/polynomial/polynomial_1"));

        // test2
//            in = new Scanner(new File("src/tests/polynomial/polynomial_2"));

        // test3
        in = new Scanner(new File("src/tests/FuncPoint/test2"));


//        in = new Scanner(new File("src/tests/FuncPoint/test2"));

        int ind = 0;
        while (in.hasNextDouble()) {
            double temp = in.nextDouble(), tems = in.nextDouble();
            x.add(temp);
            y.add(tems);
            t.add((double) ind++);
            System.out.println("f(" + temp + ")=" + tems);
        }

        System.out.println();
        System.out.println();


        xt = new CubicSpline(t, x);
        yt = new CubicSpline(t, y);


        double s = Math.abs(FuncCombine.mul(yt, xt.derivative(0, ind - 1)).integrateSimpson(0, ind - 1, 0.001));
        Sim.setText(Double.toString(s));

        FuncOnPoint f = new FuncOnPoint("yt");


        int n = 500;
        double step = (t.size() - 1) * 1. / n;
        for (int i = 0; i <= n; i++) {
            double xx = xt.calc(step * i);
            double yy = yt.calc(step * i);
            f.add(new Point2D(xx, yy));


        }


        polygon = f;
        draw = canvas.getGraphicsContext2D();
        DrPoly(polygon, draw, canvas.getWidth(), canvas.getHeight());

        CountPText.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ENTER) {
                draw.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
                DrPoly(polygon, draw, canvas.getWidth(), canvas.getHeight());
                Mon.setText(Double.toString(Monte(f, Integer.parseInt(CountPText.getText()), draw)));
            }
        });

//        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
//            double xx = event.getX();
//            double yy = event.getY();
//
//            double xo = canvas.getWidth() / 2, yo = canvas.getHeight() / 2;
////            xx -= xo;
////            yy -= yo;
//
//            double scaleX = canvas.getWidth() / (f.maxX() - f.minX()) * 0.9;
//            double scaleY = canvas.getHeight() / (f.maxY() - f.minY()) * 0.9;
//
//            draw.setLineWidth(3);
//            if (polygon.pnpoly(new Point2D((xx - xo) / scaleX, (yo - yy) / scaleY))) {
//                draw.setStroke(Color.BLUE);
//            } else {
//                draw.setStroke(Color.RED);
//            }
//            draw.strokeLine(xx, yy, xx, yy);
//        });
    }
}
