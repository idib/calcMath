package lab10;

import cMath.*;
import cMath.funcs.CubicSpline;
import cMath.funcs.Func;
import cMath.funcs.FuncCombine;
import cMath.funcs.FuncOnPoint;
import javafx.geometry.Point2D;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by idib on 19.04.17.
 */
public class lab10 {
    public static void main(String[] args) throws Exception {
        Func xt, yt;
        Scanner in;

        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();
        List<Double> t = new ArrayList<>();

        // test1
//        in = new Scanner(new File("src/tests/polynomial/polynomial_1"));

        // test2
//            in = new Scanner(new File("src/tests/polynomial/polynomial_2"));

        // test3
        in = new Scanner(new File("src/tests/FuncPoint/test1"));

        int ind = 0;
        while (in.hasNextDouble()) {
            double temp = in.nextDouble(), tems = in.nextDouble();
            x.add(temp);
            y.add(tems);
            t.add((double) ind++);
            System.out.println("f(" + temp + ")=" + tems);
        }

        System.out.println();
        System.out.println();


        xt = new CubicSpline(t, x);
        yt = new CubicSpline(t, y);


        double s = FuncCombine.mul(yt, xt.derivative(0, ind)).integrateSimpson(0, ind,0.000001);


        FuncOnPoint f = new FuncOnPoint("yt");


        int n = 500;
        double step = (t.size() - 1) * 1. / n;
        for (int i = 0; i <= n; i++) {
            double xx = xt.calc(step * i);
            double yy = yt.calc(step * i);
            f.add(new Point2D(xx, yy));


        }


        List<Func> q = new ArrayList<>();
        q.add(f);
//        f = new FuncOnPoint("12");
//        f.add(new Point2D(0,0));
//        s.add(f);
//        f = new FuncOnPoint("122");
//        f.add(new Point2D(1,1));
//        s.add(f);
//
//

        chart.srun2("", q, Collections.min(x), Collections.max(x));
    }
}