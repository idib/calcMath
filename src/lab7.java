import cMath.*;
import cMath.funcs.CubicSpline;
import cMath.funcs.Func;
import cMath.funcs.FuncEval;
import cMath.funcs.Polynomial;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Created by idib on 07.03.17.
 */
public class lab7 {
    public static void main(String[] args) throws Exception {
        NumberFormat formater = new DecimalFormat("0.#####E0##");
//        boolean fl = false;
        boolean fl = true;
        CubicSpline a;
        Polynomial b;
        Expression e = null;
        Scanner in;





        List<Double> x = new ArrayList<>();
        List<Double> y = new ArrayList<>();

        if (!fl) {
            // test1
//        in = new Scanner(new File("src/tests/polynomial/polynomial_1"));

            // test2
//            in = new Scanner(new File("src/tests/polynomial/polynomial_2"));

            // test3
            in = new Scanner(new File("src/tests/polynomial/polynomial_3"));


            while (in.hasNextDouble()) {
                double temp = in.nextDouble(), tems = in.nextDouble();
                x.add(temp);
                y.add(tems);

                System.out.println("f(" + temp + ")=" + tems);
            }
            System.out.println();
            System.out.println();
        } else {
            boolean z = true;
            while (z) {
                in = new Scanner(System.in);
                try {
                    e = new ExpressionBuilder(in.nextLine()).variable("x").build();
                    z = false;
                    System.out.println("enter x");
                    while (in.hasNextDouble()) {
                        try {
                            double temp = in.nextDouble();
                            double tems = (double) e.setVariable("x", temp).evaluate();
                            System.out.println("f(" + temp + ")=" + tems);
                            x.add(temp);
                            y.add(tems);
                        } catch (Exception es) {
                            System.out.println("incorrect x try again");
                        }
                    }
                } catch (Exception es) {
                    System.out.println("incorrect");
                }
            }
        }

        a = new CubicSpline(x, y);
        b = Polynomial.Lagrandg(x, y);

        System.out.println("Spline");
        System.out.println(a);
        System.out.println();

        List<Func> s = new ArrayList<>();
        s.add(a);
        s.add(b);
        if (e != null)
            s.add(new FuncEval("f(x)",e));

        chart.srun("", s, Collections.min(x), Collections.max(x));

        in = new Scanner(System.in);
        System.out.println("x=");
        while (in.hasNextDouble()) {
            double i = in.nextDouble();
            System.out.println(a.calc(i));
            if (e != null) {
                System.out.println("f(x)=");
                System.out.println(e.setVariable("x", i).evaluate());
            }
            System.out.println();
            System.out.println("x=");

        }
    }
}
