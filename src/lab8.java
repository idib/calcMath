import cMath.funcs.Func;
import cMath.funcs.FuncEval;
import cMath.funcs.FuncOnPoint;
import cMath.chart;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by idib on 30.03.17.
 */
public class lab8 {
    static Scanner in = new Scanner(System.in);
    static double MaxRand = (double) 0.02;
    static Random r = new Random();
    private static boolean rando = false;
//    private static boolean rando = true;

    private static List<Double> calc(Func f, double star, double end, double step) {
        int n = (int) ((end - star) / step);
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            res.add(f.calc(star + i * step));
        }
        return res;
    }

    private static List<Double> calcRandomize(List<Double> f) {
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < f.size(); i++) {
            res.add(f.get(i) + r.nextDouble() * MaxRand - MaxRand / 2);
        }
        return res;
    }

    private static List<Double> der(String promt, List f, Func ff, double star, double end, double step) {
        System.out.println("\n\n\n\n\n\n");
        System.out.println("calc " + promt);

        List<Double> fr = getX(star, end, step);
        List<Double> clearDeriv = calc(ff, star, end, step);
        List<Double> deriv;
        if (!rando)
            deriv = derivative(f, star, end, step);
        else {
            deriv = derivative(calcRandomize(f), star, end, step);
        }
//        System.out.println(dif(deriv, clearDeriv));
//        print("", deriv, clearDeriv, step, star, end);

        List<Func> funcs = new ArrayList<>();
        FuncOnPoint input = new FuncOnPoint(promt + "input", fr, clearDeriv);
        FuncOnPoint Calul = new FuncOnPoint(promt, fr, deriv);
        funcs.add(input);
        funcs.add(Calul);
        chart.srun(promt, funcs,star,end);
        return deriv;
    }

    private static List<Double> der2(String promt, List f, Func ff, double star, double end, double step) {
        System.out.println("\n\n\n\n\n\n");
        System.out.println("calc " + promt);

        List<Double> fr = getX(star, end, step);
        List<Double> clearDeriv = calc(ff, star, end, step);
        List<Double> deriv;
        if (!rando)
            deriv = derivative2(f, star, end, step);
        else {
            deriv = derivative2(calcRandomize(f), star, end, step);
        }
//        System.out.println(dif(deriv, clearDeriv));
//        print("", deriv, clearDeriv, step, star, end);

        List<Func> funcs = new ArrayList<>();
        FuncOnPoint input = new FuncOnPoint(promt + "input", fr, clearDeriv);
        FuncOnPoint Calul = new FuncOnPoint(promt, fr, deriv);
        funcs.add(input);
        funcs.add(Calul);
        chart.srun(promt, funcs,star,end);
        return deriv;
    }

    private static List<Double> derivative(List<Double> f, double star, double end, double step) {
        int n = (int) ((end - star) / step);
        List<Double> res = new ArrayList<>();
        if (f.size() >= 2) {
            res.add((f.get(1) - f.get(0)) / step);
        }
        for (int i = 1; i < n; i++) {
            res.add((f.get(i) - f.get(i - 1)) / step);
        }
        return res;
    }

    private static List<Double> derivative2(List<Double> f, double star, double end, double step) {
        int n = (int) ((end - star) / step);
        List<Double> res = new ArrayList<>();
        if (f.size() >= 2) {
            res.add(f.get(0));
        }
        for (int i = 1; i < n - 1; i++) {
            res.add((f.get(i - 1) - 2 * f.get(i) + f.get(i + 1)) / step / step);
        }
        if (f.size() >= 2) {
            res.add(f.get(0));
        }
        return res;
    }

    private static double dif(List<Double> deriv, List<Double> clearDeriv) {
        double res = 0;
        for (int i = 0; i < deriv.size(); i++) {
            if (i == 0 || Math.abs(deriv.get(i) - clearDeriv.get(i)) > res) {
                res = Math.abs(deriv.get(i) - clearDeriv.get(i));
            }
        }
        return res;
    }

    private static Expression enterFX(String promt) {
        while (true) {
            try {
                System.out.println(promt);
                return new ExpressionBuilder(in.nextLine()).variable("x").build();

            } catch (Exception es) {
                System.out.println("incorrect");
            }
        }
    }

    private static List<Double> getX(double star, double end, double step) {
        int n = (int) ((end - star) / step);
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            res.add(star + i * step);
        }
        return res;
    }

    public static void main(String[] args) throws Exception {
        FuncEval fx, ffx, fsx, ftx;
        fx = new FuncEval("f(x)=", enterFX("f(x)="));
        ffx = new FuncEval("f'(x)=", enterFX("f'(x)="));
        fsx = new FuncEval("f''(x)=", enterFX("f''(x)="));
        ftx = new FuncEval("f'''(x)=", enterFX("f'''(x)="));
        System.out.println("enter start x");
        double startx = in.nextDouble();
        System.out.println("enter end x");
        double endx = in.nextDouble();
        double step = (double) 0.1;


        List<Func> tete = new ArrayList<>();
        tete.add(fx);
        tete.add(ffx);
        tete.add(fsx);
        tete.add(ftx);


        chart.srun("input", tete, startx, endx);


        List<Double> rte = der("f'(x)", calc(fx, startx, endx, step), ffx, startx, endx, step);
        rte = der("f''(x)", rte, fsx, startx, endx, step);
//        rte = der2("f'' var 2(x)", calc(fx, startx, endx, step), fsx, startx, endx, step);
        der("f'''(x)", rte, ftx, startx, endx, step);
    }

    private static void print(String promt, List<Double> f, List<Double> ff, int index, double star, double end) {
        print(promt, f, ff, (end - star) / index, star, end);
    }

    private static void print(String promt, List<Double> f, List<Double> ff, double step, double star, double end) {
        System.out.println("\n\n");
        System.out.println(promt + " for " + step);
        System.out.println("Max dif=" + dif(f, ff));
        System.out.println("x = ");
        int n = (int) ((end - star) / step);
        List<Double> res = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            res.add(star + i * step);
        }
        System.out.println(res);
        System.out.println("f cal = ");
        System.out.println(f);
        System.out.println("f orig = ");
        System.out.println(ff);
    }
}