import cMath.Matrix;

import java.io.FileNotFoundException;

/**
 * Created by idib on 03.02.17.
 */
public class lab2 {
    public static void main(String[] args) throws FileNotFoundException {
        Matrix A, B;

        // test1
        A = new Matrix("src/tests/matrix/matrix_sim_1");
        B = new Matrix("src/tests/matrix/matrix_b_1");

        //test2
//        A = new Matrix("src/tests/matrix/matrix_sim_2");
//        B = new Matrix("src/tests/matrix/matrix_b_1");

        //test3
//        A = new Matrix("src/tests/matrix/matrix_sim_3");
//        B = new Matrix("src/tests/matrix/matrix_b_1");


        System.out.println("A=");
        System.out.println(A.toString());
        System.out.println();


        System.out.println("B=");
        System.out.println(B.toString());
        System.out.println();


        double det = A.det();
        System.out.println("det(A)=");
        System.out.println(det);
        System.out.println();
        if (det != 0) {
            Matrix At =  A.clone();
            At.addColl(B);
            At = At.transformTrapezoidWithMax();
            Matrix X = At.Gauss();
            Matrix AX = Matrix.mul(A, X);
            Matrix BAX = Matrix.sub(B, AX);

            System.out.println("Gauss");
            System.out.println("X=");
            System.out.println(X.toString());
            System.out.println();

            System.out.println("B - AX =");
            System.out.println(BAX.toString());

            Matrix X1 = A.sqrtMetod(B);
            Matrix AX1 = Matrix.mul(A, X1);
            Matrix BAX1 = Matrix.sub(B, AX1);



            System.out.println("sqrt metod");
            System.out.println("X=");
            System.out.println(X1.toString());
            System.out.println();

            System.out.println("B - AX =");
            System.out.println(BAX1.toString());
            System.out.println();


            Matrix delta = Matrix.sub(X.abs(),X1.abs()).abs();
            System.out.println("delta for X in medod sqrt and Gauss =");
            System.out.println(delta);

            Matrix delta1 = Matrix.sub(BAX.abs(),BAX1.abs()).abs();
            System.out.println("delta for B - AX in medod sqrt and Gauss =");
            System.out.println(delta1);
        }
    }
}
