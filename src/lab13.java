import cMath.RungeKutt;
import cMath.chart;
import cMath.funcs.Func;
import cMath.funcs.FuncOnPoint;
import javafx.geometry.Point2D;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class lab13 {

    protected static int size = 2;
    //    private static boolean flag_auto = false;
    private static boolean flag_auto = true;

    public static void main(String[] args) throws IOException {
        sys s = new sys();
        FuncOnPoint[] Y1;
        FuncOnPoint[] Y2 = new FuncOnPoint[size];
        for (int i = 0; i < Y2.length; i++)
            Y2[i] = new FuncOnPoint("y" + i);

        double start= 0;
        double finish= 15;
//        Scanner input = new Scanner(System.in);
//        System.out.println("x start= ");
//        start = input.nextDouble();
//        System.out.println("x end=");
//        finish = input.nextDouble();
        int count = 0;

        s.YStart(Y2);
        if (!flag_auto) {
//            System.out.println("n= ");
//            count = input.nextInt();
            s.Meth_Const(Y2, start, finish, count);
        } else {
//            System.out.println("eps=");
            double eps = 0.0001;
//            eps = input.nextDouble();
            count = s.Meth_Auto(Y2, start, finish, eps);
        }

        double norm_pogresh = 0;
//        Y1 = Solution(Y2);
//        for (int i = 0; i < count; i++)
//            norm_pogresh = Math.max(norm_pogresh, Math.abs(Y2[0].get(i).getY() - Y1[0].get(i).getY()));

        chart.srun("", new ArrayList<Func>(Arrays.asList(Y2)), start, finish);

        NumberFormat formater = new DecimalFormat("0.#####E0##");
        String[] str = new String[count];
        for (int i = 0; i < count; i++)
            str[i] = Y2[0].get(i).getX() + "\t";
        for (int j = 0; j < size; j++)
            for (int i = 0; i < count; i++)
                str[i] += Y2[j].get(i).getY() + "\t";
        System.out.println("X\tY calc\tY orig");
        for (int i = 0; i < count; i++)
            System.out.println(str[i]);
    }

    static class sys extends RungeKutt {
        public sys() {
            size = 2;
        }

        @Override
        protected FuncOnPoint[] Solution(FuncOnPoint[] funcs) {
            //double[] y = new double[size];
            FuncOnPoint[] res = new FuncOnPoint[funcs.length];
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
//        for (Point2D p : funcs[0]) {
//            res[0].add(new Point2D(/**/,p.getX()));
//        }
            return res;
        }

        @Override
        protected double[] SystemFunc(double x, double[] y) {
//        return new double[]{
//                -2*y[0]+4*y[1],
//                -y[0]+3*y[1]};
            return new double[]{
                    y[1],
                    -y[0]};
        }

        @Override
        protected void YStart(FuncOnPoint[] f) {
//        f[0].add(new Point2D(0, 3));
//        f[1].add(new Point2D(0, 0));
            f[0].add(new Point2D(0, 1));
            f[1].add(new Point2D(0, 1));

        }
    }
}
