import cMath.chart;
import cMath.funcs.Func;
import cMath.funcs.FuncEval;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by idib on 11.04.17.
 */
public class lab11 {

    static Scanner in = new Scanner(System.in);
    static double MaxRand = (double) 0.02;
    static Random r = new Random();
    private static boolean rando = false;
//    private static boolean rando = true;

    private static Expression enterFX(String promt) {
        while (true) {
            try {
                System.out.println(promt);
                return new ExpressionBuilder(in.nextLine()).variable("x").build();

            } catch (Exception es) {
                System.out.println("incorrect");
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Func fx, ffx, fffx;
        fx = new FuncEval("f(x)=", enterFX("f(x)="));
        ffx = new FuncEval("f'(x)=", enterFX("f'(x)="));
        fffx = new FuncEval("f''(x)=", enterFX("f''(x)="));
        while (true) {
            System.out.println("enter start x");
            System.out.println("enter end x");
            double startx = in.nextDouble();
            double endx = in.nextDouble();
            List<Func> l = new ArrayList<>();
            l.add(fx);
            chart.srun("plot", l, startx, endx);
            double step = fx.step;
            long n = (long) (Math.abs(startx - endx) / step);
            boolean sign = ffx.calc(startx) >= 0;
            List<Double> p = new ArrayList<>();
            p.add(startx + .1);
            for (long i = 0; i < n; i++) {
                boolean temp = ffx.calc(startx + i * step) >= 0;
                if (sign ^ temp)
                    p.add(startx + (i - 0.5) * step);
                sign = temp;
            }
            p.add(endx - 0.1);

            for (int i = 1; i < p.size(); i++) {
                startx = p.get(i - 1);
                endx = p.get(i);

                double s;
                System.out.println("\n\n\n" + startx + ' ' + endx + '\n');
//                System.out.println("Secant eps = " + fx.eps);
//                s = fx.findXСhord(startx, endx,ffx, fffx);
//                System.out.println("count Itration");
//                System.out.println(fx.countIter);
//                System.out.println("f(x) = 0  x =" + s);
//                System.out.println("\n\n\n");

                s = fx.findXSecant(startx, endx);
                System.out.println("eps = " + fx.eps);
                System.out.println("count Itration");
                System.out.println(fx.countIter);
                System.out.println("f(x) = 0  x =" + s);
                System.out.println("\n\n\n");
            }
        }
    }
}
