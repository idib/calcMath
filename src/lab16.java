import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by idib on 22.05.17.
 */
public class lab16 {
    //входные константы
    private static final int I = 10, J = 20;//число разбиений: по X И по Y соответсвенно
    private static double a = 1, b = Math.PI / 2; //прямоугольная область со сторонами: 0 < x < 2, 0 < y < 1
    private static int k_j = 0, k_p = 0; //число итераций для методов простых итераций и переменных направлений соответственно
    private static double tau; //итерационный параметр
    private static double eps; //точность вычислений
    private static double h1, h2; //шаг по X И по Y соответсвенно

    //массивы с хранением решений
    private static double[][]
            //для метода переменных направлений
            u1 = new double[I][J], //Матрица на k-1 шаге
            u05 = new double[I][J], //Матрица на половинчатом шаге
            u2 = new double[I][J], //Матрица на к шаге

    //Для метода простых итераций
    u1_j = new double[I][J], //Матрица на половинчатом шаге
            u2_j = new double[I][J]; //Матрица на к шаге
    private static PrintWriter sr;

    //ОШИБКА АППРОКСИМАЦИИ
    public static double error(double[][] array) {
        double[][] err = new double[I][J];
        double max = -100;

        for (int i = 0; i < I; i++) {
            for (int j = 0; j < J; j++) {
                err[i][j] = Math.abs(right_solution(i * h1, j * h2) - array[i][j]);
            }
        }

        for (int i = 0; i < I; i++) {
            for (int j = 0; j < J; j++) {
                if (err[i][j] > max)
                    max = err[i][j];
            }
        }

        return max;
    }

    //ПРАВАЯ ЧАСТЬ УРАВНЕНИЯ ПУАССОНА: -f(x,y)
    public static double f(double x, double y) {
        return 0;
    }

    //ЗНАЧЕНИЯ НА ГРАНИЦАХ ОБЛАСТИ
    //верхняя строка
    public static double g1(double x, double y) {
        return Math.sin(4 * y);
    }

    //нижняя строка
    public static double g2(double x, double y) {
        return Math.exp(4) * Math.sin(4 * y);
    }

    //левый столбец
    public static double g3(double x, double y) {
        return 0;
    }

    //правый столбец
    public static double g4(double x, double y) {
        return Math.exp(4 * x) * Math.sin(2 * Math.PI);
    }

    //ВЫЧИСЛЕНИЕ МЕТОДОМ ПРОСТЫХ ИТЕРАЦИЙ
    public static void jacobi() {
        //ЗАБИВАЕМ УСЛОВИЯ
        //условие по игрекам
        for (int i = 0; i < J; i++) {
            u1_j[0][i] = g1(0, h2 * i);         //первая и последняя строка: игреки
            u1_j[I - 1][i] = g2(2, h2 * i);
            u2_j[0][i] = g1(0, h2 * i);
            u2_j[I - 1][i] = g2(2, h2 * i);
        }
        //условие по иксам
        for (int i = 0; i < I; i++) {
            u1_j[i][0] = g3(h1 * i, 0);         //первый и последний столбец: иксы
            u1_j[i][J - 1] = g4(h1 * i, 1);
            u2_j[i][0] = g3(h1 * i, 0);
            u2_j[i][J - 1] = g4(h1 * i, 1);
        }

        //начальное приближение
        for (int i = 1; i < I - 1; i++) {
            for (int j = 1; j < J - 1; j++) {
                u1_j[i][j] = 1;
                u2_j[i][j] = 2;
            }
        }

        //Якоби
        double norma = 1;
        while (norma > eps) {
            for (int i = 1; i < I - 1; i++) {
                //запоминаем предыдущее приближении
                for (int j = 1; j < J - 1; j++) {
                    u1_j[i][j] = u2_j[i][j];
                }
            }
            for (int i = 1; i < I - 1; i++) {
                for (int j = 1; j < J - 1; j++) {
                    //u2_j[i][j] = ((h2 * h2) / (2 * (h1 * h1 + h2 * h2))) * (u2_j[i - 1, j] + u2_j[i + 1, j])
                    //+ ((h1 * h1) / (2 * (h1 * h1 + h2 * h2))) * (u2_j[i][j - 1] + u2_j[i][j + 1])
                    //+ ((h1 * h1 * h2 * h2) / (2 * (h1 * h1 + h2 * h2))) * f(i * h1, j * h2);

                    u2_j[i][j] = ((h2 * h2) / (2 * (h1 * h1 + h2 * h2))) * (u1_j[i - 1][j] + u1_j[i + 1][j])
                            + ((h1 * h1) / (2 * (h1 * h1 + h2 * h2))) * (u1_j[i][j - 1] + u1_j[i][j + 1])
                            + ((h1 * h1 * h2 * h2) / (2 * (h1 * h1 + h2 * h2))) * f(i * h1, j * h2);
                }
            }
            norma = norm(u2_j, u1_j);
            k_j++;
        }
        ;
    }

    //ЗАГРУЗКА ФОРМЫ
    public static void main(String[] args) throws IOException {
        File outs = new File("output");
        outs.createNewFile();
        sr = new PrintWriter(outs);

        //вычисляем шаги по сетке
        h1 = a / (I - 1); //шаг по x
        h2 = b / (J - 1); //шаг по y
        tau = (a * b * Math.sqrt(h1 * h1 + h2 * h2)) / (3.14 * Math.sqrt(a * a + b * b));

        //вычисляем точность
        eps = 0.1 * ((h1 + h2) / 2) * ((h1 + h2) / 2);

        //ЗАБИВАЕМ УСЛОВИЯ
        //условие по игрекам
        for (int i = 0; i < J; i++) {
            u1[0][i] = g1(0, h2 * i);         //первая и последняя строка: игреки
            u1[I - 1][i] = g2(2, h2 * i);
            u2[0][i] = g1(0, h2 * i);
            u2[I - 1][i] = g2(2, h2 * i);
            u05[0][i] = g1(0, h2 * i);
            u05[I - 1][i] = g2(2, h2 * i);
        }
        //условие по иксам
        for (int i = 0; i < I; i++) {
            u1[i][0] = g3(h1 * i, 0);         //первый и последний столбец: иксы
            u1[i][J - 1] = g4(h1 * i, 1);
            u2[i][0] = g3(h1 * i, 0);
            u2[i][J - 1] = g4(h1 * i, 1);
            u05[i][0] = g3(h1 * i, 0);
            u05[i][J - 1] = g4(h1 * i, 1);
        }

        //начальное приближение
        for (int i = 1; i < I - 1; i++) {
            for (int j = 1; j < J - 1; j++) {
                u1[i][j] = 1;
                u2[i][j] = 2;
                u05[i][j] = 1;
            }
        }

        //ПРОДОЛЬНО-ПОПЕРЕЧНАЯ ПРОГОНКА
        double norma = 1;
        while (norma > eps) {
            //изменение начального приближения
            for (int i = 1; i < I - 1; i++) {
                for (int j = 1; j < J - 1; j++) {
                    u1[i][j] = u2[i][j];
                }
            }

            //прогонка по строкам: j зафиксировать и прогнать по i
            for (int j = 1; j < I - 1; j++)
                for (int i = 1; i < J - 1; i++)
                    progonka05(i, j);

            //прогонка по столбцам: i зафиксировать и прогонать по j
            for (int i = 1; i < I - 1; i++)
                for (int j = 1; j < J - 1; j++)
                    progonka2(i, j);

            norma = norm(u2, u1);
            k_p++;
        }
        ;

        jacobi();

        //вывод ответа
        output();
    }

    //ВЫЧИСЛЕНИЕ НОРМЫ ОСТАНОВКИ ИТЕРАЦИОННОГО ПРОЦЕССА
    public static double norm(double[][] u1_n, double[][] u2_n) {
        //u1_n - на текущем шаге, u2_n - на предыдущем шаге
        double[][] u3 = new double[I][J];
        double max, max_pred;
        int i, j;

        //норма на предыдущ. шаге
        max_pred = -100;
        for (i = 0; i < I; i++) {
            for (j = 0; j < J; j++) {
                if (u2_n[i][j] > max_pred)
                    max_pred = u2_n[i][j];
            }
        }

        //норма разности
        for (i = 0; i < I; i++) {
            for (j = 0; j < J; j++) {
                u3[i][j] = Math.abs(u1_n[i][j] - u2_n[i][j]);
            }
        }
        max = -100;
        for (i = 0; i < I; i++) {
            for (j = 0; j < J; j++) {
                if (u3[i][j] > max)
                    max = u3[i][j];
            }
        }

        //результирующая норма
        max = max / max_pred;

        return max;
    }

    //ВЫВОД ОТВЕТА
    private static void output() {
        sr.print("JACOBI METHOD");
        sr.print("     Error: ");
        sr.print(error(u2_j));
        sr.print("     Iteracii: ");
        sr.print(k_j);
        sr.println();
        for (int i = 0; i < I; i++) {
            for (int j = 0; j < J; j++) {
                sr.print(u2_j[i][j]);
                sr.print("\t");
            }
            sr.println("");
        }

        u2[I - 1][J - 1] = g4(h1 * (I - 1), 1);
        sr.print("ADI METHOD");
        sr.print("     Error: ");
        sr.print(error(u2));
        sr.print("     Iteracii: ");
        sr.print(k_p);
        sr.println();
        for (int i = 0; i < I; i++) {
            for (int j = 0; j < J; j++) {
                sr.print(u2[i][j]);
                sr.print("\t");
            }
            sr.println("");
        }

        sr.println("RIGHT SOLUTION");
        for (int i = 0; i < I; i++) {
            for (int j = 0; j < J; j++) {
                sr.print(right_solution(i * h1, j * h2));
                sr.print("\t");
            }
            sr.println("");
        }
        sr.close();

    }

    //ПРОГОНКА
    public static void progonka05(int ii, int jj) {
        //объявления вспом списков
        //прогоночные коэффициенты

        double[] alp = new double[I + 2];
        double[] bet = new double[I + 2];
        //коээфиенты при неизвестных
        double[] a = new double[I + 1];
        double[] b = new double[I + 1];
        double[] c = new double[I + 1];
        double[] d = new double[I + 1];

        //иниц-я нулям нач списков

        a[0] = -(tau) / (2 * h1 * h1);
        b[0] = (1 + tau / (h1 * h1)); //убрать минус?
        c[0] = -(tau / (2 * h1 * h1));

        //заполняем коэффициенты A, B, C
        for (int i = 1; i < I; i++) {
            a[i] = -(tau / (2 * h1 * h1));
            b[i] = -(1 + tau / (h1 * h1));
            c[i] = -(tau / (2 * h1 * h1));
        }

        d[0] = tau / 2 * f(0 * h1, 0 * h2) + tau / 2 * h2 * h2 * u1[0][0] + (1 - tau / h2 * h2) * u1[0][0]
                + tau / 2 * h2 * h2 * u1[0][0];

        int iii = 1;
        for (int i = 1; i < I - 1; i++) {
            for (int j = 1; j < J - 1; j++)
                d[i] = tau / 2 * f(i * h1, j * h2) + tau / 2 * h2 * h2 * u1[i][j - 1] + (1 - tau / h2 * h2) * u1[i][j]
                        + tau / 2 * h2 * h2 * u1[i][j + 1];
            iii++;
        }

        d[I - 1] = tau / 2 * f(I - 1 * h1, J - 1 * h2) + tau / 2 * h2 * h2 * u1[I - 1][J - 1]
                + (1 - tau / h2 * h2) * u1[I - 1][J - 1] + tau / 2 * h2 * h2 * u1[I - 1][J - 1];

        alp[1] = c[0] / b[0];
        bet[1] = -d[0] / b[0];

        //Вычисляем коэффициенты прогонки
        for (int i = 1; i < I + 1; i++) {
            alp[i + 1] = c[i] / (b[i] - a[i] * alp[i]);
            bet[i + 1] = (a[i] * bet[i] - d[i]) / (b[i] - a[i] * alp[i]);
        }

        u05[I - 1][J - 1] = bet[I + 1];

        //Обратный ход прогонки: находим неизвестные
        for (int i = I - 2; i >= 1; i--)
            for (int j = J - 2; j >= 1; j--)
                u05[i][j] = alp[i + 1] * u05[i + 1][j] + bet[i + 1];
    }

    //сделать по J???
    public static void progonka2(int ii, int jj) {
        //объявления вспом списков
        //прогоночные коэффициенты

        double[] alp = new double[I + 2];
        double[] bet = new double[I + 2];
        //коээфиенты при неизвестных
        double[] a = new double[I + 1];
        double[] b = new double[I + 1];
        double[] c = new double[I + 1];
        double[] d = new double[I + 1];

        a[0] = -(tau) / (2 * h2 * h2);
        b[0] = -(1 + tau / (h2 * h2)); //убрать минус?
        c[0] = -(tau / (2 * h2 * h2));

        //заполняем коэффициенты A, B, C
        for (int i = 1; i < I; i++) {
            a[i] = -(tau / (2 * h2 * h2));
            b[i] = -(1 + tau / (h2 * h2));
            c[i] = -(tau / (2 * h2 * h2));
        }

        d[0] = tau / 2 * f(0 * h1, 0 * h2) + tau / 2 * h1 * h1 * u05[0][0] + (1 - tau / h1 * h1) * u05[0][0]
                + tau / 2 * h1 * h1 * u05[0][0];

        //приравнять d к нач. условиям u1
        int iii = 1;
        for (int i = 1; i < I - 1; i++) {
            for (int j = 1; j < J - 1; j++)
                d[i] = tau / 2 * f(i * h1, j * h2) + tau / 2 * h1 * h1 * u05[i][j - 1] + (1 - tau / h1 * h1) * u05[i][j]
                        + tau / 2 * h1 * h1 * u05[i][j + 1];
            iii++;
        }

        d[I - 1] = tau / 2 * f(I - 1 * h1, J - 1 * h2) + tau / 2 * h1 * h1 * u05[I - 1][J - 1]
                + (1 - tau / h1 * h1) * u05[I - 1][J - 1] + tau / 2 * h1 * h1 * u05[I - 1][J - 1];

        alp[1] = c[0] / b[0];
        bet[1] = -d[0] / b[0];

        //Вычисляем коэффициенты прогонки
        for (int i = 1; i < I + 1; i++) {
            alp[i + 1] = c[i] / (b[i] - a[i] * alp[i]);
            bet[i + 1] = (a[i] * bet[i] - d[i]) / (b[i] - a[i] * alp[i]);
        }

        u2[I - 1][J - 1] = bet[I + 1];

        //Обратный ход прогонки: находим неизвестные
        for (int i = I - 2; i >= 1; i--)
            for (int j = J - 2; j >= 1; j--)
                u2[i][j] = alp[i + 1] * u2[i + 1][j] + bet[i + 1];
    }

    //ТОЧНОЕ РЕШЕНИЕ
    public static double right_solution(double x, double y) {
        return Math.exp(4 * x) * Math.sin(4 * y);
    }

    //ВЫЧИСЛЕНИЕ НОРМЫ ОСТАНОВКИ ИТЕРАЦИОННОГО ПРОЦЕССА
    public double norm1(double[][] u1_n, double[][] u2_n) {
        //u1_n - на текущем шаге, u2_n - на предыдущем шаге
        double[][] u3 = new double[I][J];
        double max;
        int i, j;

        for (i = 0; i < I; i++) {
            for (j = 0; j < J; j++) {
                u3[i][j] = Math.abs(u1_n[i][j] - u2_n[i][j]);
            }
        }

        max = -100;
        for (i = 0; i < I; i++) {
            for (j = 0; j < J; j++) {
                if (u3[i][j] > max)
                    max = u3[i][j];
            }
        }
        return max;
    }

}
