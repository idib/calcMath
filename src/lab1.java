import cMath.Matrix;

import java.io.FileNotFoundException;

/**
 * Created by idib on 03.02.17.
 */
public class lab1 {

    public static void main(String[] args) throws FileNotFoundException {
        boolean fl;
        Matrix A, B;

        fl = true;
//        fl=false;


        // test1
        A = new Matrix("src/tests/matrix/matrix_a_1");
        B = new Matrix("src/tests/matrix/matrix_b_1");

        //test2
//        A = new Matrix("src/tests/matrix/matrix_a_2");
        //test21
//        B = new Matrix("src/tests/matrix/matrix_b_2_1");
        //test22
//        B = new Matrix("src/tests/matrix/matrix_b_2_2");
        //test23
//        B = new Matrix("src/tests/matrix/matrix_b_2_3");

        //test3
//        A = new Matrix("src/tests/matrix/matrix_a_3");
//        B = new Matrix("src/tests/matrix/matrix_b_3");

        //test4

//        A = new Matrix("src/tests/matrix/matrix_a_4");
//        B = new Matrix("src/tests/matrix/matrix_b_4");

        System.out.println("A=");
        System.out.println(A.toString());
        System.out.println();

        double det = A.det();

        System.out.println("det(A)=");
        System.out.println(det);
        System.out.println();

        if (det != 0) {
            System.out.println("B=");
            System.out.println(B.toString());
            System.out.println();


            Matrix At = A.clone();
            At.addColl(B);
            if (fl) {
                At = At.transformTrapezoidWithMax();
                System.out.println("transform Trapezoid with Max");
            } else {
                At = At.transformTrapezoid();
                System.out.println("transform Trapezoid");
            }

            System.out.println("At=");
            System.out.println(At.toString());
            System.out.println();

            Matrix X = At.Gauss();

            System.out.println("X=");
            System.out.println(X.toString());
            System.out.println();

            Matrix AX = Matrix.mul(A, X);


            Matrix BAX = Matrix.sub(B, AX);

            System.out.println("B - AX=");
            System.out.println(BAX.toString());
            System.out.println();
        }
    }
}
