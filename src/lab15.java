import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by idib on 22.05.17.
 */
public class lab15 {
    private static int K = 20; //число разбиений: по t и по x соответсвенно
    private static double a = Math.sqrt(2); //коэффициент теплопроводности
    private static double l = Math.PI/2; //изменение аргумента x: 0 < x < l
    private static double T; //изменение времени t: 0 < t < T
    private static double timing = 0; //время t по слоям
    private static double alpha1 = 3.0;
    private static double beta1 = 1.0; //альфа и бета для граничного условия u(t,0)
    private static double alpha2 = 0.0;
    private static double beta2 = 1.0; //альфа и бета для граничного условия u(t,l)
    private static double r; //условие устойчивости явной схемы
    private static int k_yavn = 0;
    private static int k_neyavn = 0; //счетчики слоев для явной схемы и неявной схемы
    private static double tau; //шаг по t
    private static double h; //шаг по x
    private static double sigma; //вещественный параметр, вес
    private static PrintWriter sr;
    //массивы с хранением решений
    private static double[]
            //для явной схемы
            y1_yavn = new double[K], //Матрица на n итерации
            y2_yavn = new double[K], //Матрица на n+1 итерации
    //для неявной схемы
    y1_neyavn = new double[K], //Матрица на n итерации
            y2_neyavn = new double[K]; //Матрица на n+1 итерации
    //входные константы
    private int N = 10;

    //ОШИБКА АППРОКСИМАЦИИ
    public static double error(double[] array, double new_timing) {
        double[] err = new double[K];
        double max = -100;

        for (int k = 0; k < K; k++)
            err[k] = Math.abs(right_solution(k * h, new_timing) - array[k]);

        for (int k = 0; k < K; k++) {
            if (err[k] > max)
                max = err[k];
        }

        return max;
    }

    //ЗАГРУЗКА ФОРМЫ
    public static void main(String[] args) throws IOException {
        File out = new File("output");
        out.createNewFile();
        sr = new PrintWriter(out);
        //вычисление по неявной схеме
        neyavn();
        //вычисление по неявной схеме
        yavn();

        sr.close();
    }

    //ВЫВОД РЕШЕНИЯ ПО ОДНОМУ СЛОЮ ДЛЯ НЕЯВНОЙ СХЕМЫ
    private static void output_neyavn(double new_timing) {
        sr.print("SLOY: ");
        sr.print(k_neyavn);
        sr.println("");
        sr.print("Error: ");
        sr.print(error(y2_neyavn, new_timing));
        sr.print("    timing: ");
        sr.print(new_timing);
        sr.print("    tau: ");
        sr.print(tau);
        sr.print("    h: ");
        sr.print(h);
        sr.println();

        sr.print("Neyavniy method  ");
        for (int k = 0; k < K; k++) {
            sr.print("\t");
            sr.print(y2_neyavn[k]);
        }

        sr.println("");
        sr.print("Right solution ");
        for (int k = 0; k < K; k++) {
            sr.print("\t");
            sr.print(right_solution(k * h, new_timing));

        }
        sr.println("\n");
    }

    //ВЫВОД РЕШЕНИЯ ПО ОДНОМУ СЛОЮ ДЛЯ ЯВНОЙ
    private static void output_yavn(double new_timing) {
        sr.print("SLOY: ");
        sr.print(k_yavn);
        sr.println("");
        sr.print("Error: ");
        sr.print(error(y2_yavn, new_timing));
        sr.print("    timing: ");
        sr.print(new_timing);
        sr.print("    tau: ");
        sr.print(tau);
        sr.print("    h: ");
        sr.print(h);
        sr.println();

        sr.print("Yavniy method  ");
        for (int k = 0; k < K; k++) {
            sr.print("\t");
            sr.print(y2_yavn[k]);
        }

        sr.println("");
        sr.print("Right solution ");
        for (int k = 0; k < K; k++) {
            sr.print("\t");
            sr.print(right_solution(k * h, new_timing));

        }
        sr.println("\n");
    }

    //ПРОГОНКА: альфа1 = 1, альфа2 = 0
    public static void progonka() {
        //инициализация коэффициентов
        double delta1, ksi1, v1,
                delta2, ksi2, v2;

        //объявления вспомагательных списков
        //прогоночные коэффициенты
        double[] alp = new double[K + 2];
        double[] bet = new double[K + 2];
        //коээфиенты при неизвестных
        double[] A = new double[K + 1];
        double[] B = new double[K + 1];
        double[] C = new double[K + 1];
        double[] D = new double[K + 1];

        //заполняем коэффициенты A, B, C
        for (int k = 0; k < K; k++) {
            A[k] = a * a * sigma;
            B[k] = 2 * a * a * sigma + (h * h) / tau;
            C[k] = a * a * sigma;
        }

        //заполняем коэффициент D: проверить индексы y1_neyavn
        D[0] = (2 * a * a * (1 - sigma) - h * h / tau) * y1_neyavn[0] - a * a * (1 - sigma) * (y1_neyavn[0] + y1_neyavn[0]);
        for (int k = 1; k < K - 1; k++)
            D[k] = (2 * a * a * (1 - sigma) - h * h / tau) * y1_neyavn[k] - a * a * (1 - sigma) * (y1_neyavn[k - 1] + y1_neyavn[k + 1]); //тут фи добавить, но у меня f нулю равна
        D[K - 1] = (2 * a * a * (1 - sigma) - h * h / tau) * y1_neyavn[K - 1] - a * a * (1 - sigma) * (y1_neyavn[K - 1] + y1_neyavn[K - 1]);

        //вычисляем первые коэффициенты прогонки
        alp[1] = C[0] / B[0];
        bet[1] = -D[0] / B[0];

        //Вычисляем коэффициенты прогонки
        for (int i = 1; i < K + 1; i++) {
            alp[i + 1] = C[i] / (B[i] - A[i] * alp[i]);
            bet[i + 1] = (A[i] * bet[i] - D[i]) / (B[i] - A[i] * alp[i]);
        }

        //коэффициенты уравнений системы
        //альфа1 = 1
        delta1 = sigma * (alpha1 + h * beta1) + (alpha1 * h * h) / (2 * a * a * tau);
        ksi1 = (alpha1 * sigma) / delta1;
        v1 = 1.0 / delta1 * (h * mu1(timing) + (1 - sigma) * alpha1 * y1_neyavn[1] +
                y1_neyavn[0] * ((alpha1 * h * h) / (2 * a * a * tau) - (1 - sigma) * (alpha1 + beta1 * h)));
        //альфа2 = 0
        delta2 = sigma * (alpha2 + h * beta2) + (alpha2 * h * h) / (2 * a * a * tau);
        ksi2 = 0;
        v2 = mu2(timing) / beta2;

        //конечное значение
        y2_neyavn[K - 1] = ksi2 * y2_neyavn[K - 2] + v2;

        //Обратный ход прогонки: находим неизвестные
        for (int k = K - 2; k >= 1; k--)
            y2_neyavn[k] = alp[k + 1] * y2_neyavn[k + 1] + bet[k + 1];

        //начальное значение
        y2_neyavn[0] = ksi1 * y2_neyavn[1] + v1;
    }

    //Функция - слагаемое в уравнении теплопроводности
    static double f(double t, double x) {
        return 2 * Math.exp(-2 * t) * Math.cos(x);
    }

    //Мю для граничного условия u(t,0)
    static double mu1(double t) {
        return (1 + 2 * t) * Math.exp(-2 * t);
    }

    //Мю для граничного условия u(t,l)
    static double mu2(double t) {
        return 0;
    }

    //НЕЯВНАЯ СХЕМА
    static void neyavn() {
        sigma = 0.1; //вес параметра для неявной схемы
        timing = 0;
        T = 0.3;
        h = l / K; //шаг по x
        tau = h * h / (2 * a * a);

        //Нулевой слой
        for (int k = 0; k < K; k++) {
            y1_neyavn[k] = u0(k * h);
            y2_neyavn[k] = u0(k * h);
        }
        //вывод нулевого слоя
        //output_neyavn(timing);

        k_neyavn++; //теперь от первого слоя считаем

        //Собственно неявная сxема: вычисляем значение на новом слое через предыдущий  слой
        while (timing < T) {
            timing = timing + tau; //шаг тайминга: выбираем следующий слой

            //новый слой в старый
            for (int k = 0; k < K; k++)
                y1_neyavn[k] = y2_neyavn[k];

            progonka();

            //вывод текущего слоя
            //if (k_neyavn == 3)
            output_neyavn(timing);

            k_neyavn++;
        }
        ;

        sr.println("************************************************************************************************************************");
    }

    //Точное решение
    static double right_solution(double x, double t) {
        return (1+2*t)* Math.exp(-2*t)*Math.cos(x);
    }

    //Начальное условие при t = 0
    static double u0(double x) {
        return Math.cos(x);
    }

    //ЯВНАЯ СХЕМА
    static void yavn() {
        T = 0.1; //изменение времени t: 0 < t < T
        timing = 0;
        h = l / K; //шаг по x
        sigma = 0; //вес параметра для явной схемы
        tau = h * h / (2 * a * a);
        r = (a * a * tau) / (h * h); //параметр (условие сходимости явной схемы)
        //r = 1.0;

        //Нулевой слой: нужно ли переопределить гранич условия? вроде, все сходится и так
        for (int k = 0; k < K; k++) {
            y1_yavn[k] = u0(k * h);
            y2_yavn[k] = u0(k * h);
        }
        //вывод нулевого слоя
        //output_yavn(timing);

        k_yavn++; //теперь от первого слоя считаем

        //Собственно явная сxема: вычисляем значение на новом слое через предыдущий слой
        while (timing < T) {
            timing = timing + tau; //шаг тайминга: выбираем следующий слой

            //новый слой в старый
            for (int k = 0; k < K; k++)
                y1_yavn[k] = y2_yavn[k];

            for (int k = 1; k < K - 1; k++)
                //y2_yavn[k] = tau / (h * h) * (y1_yavn[k + 1] - 2 * y1_yavn[k] + y1_yavn[k - 1]) + y1_yavn[k];
                y2_yavn[k] = (1 - 2 * r) * y1_yavn[k] + r * (y1_yavn[k - 1] + y1_yavn[k + 1]) + tau * f(timing, k * h); //через параметр r

            //аппроксимация первого граничного условия
            y2_yavn[0] = (2 * h * tau * mu1(timing) + h * h * y1_yavn[0] + 2 * tau * y2_neyavn[1]) / (2 * tau + 2 * tau * h + h);

            //точная аппроксимация второго граничного условия
            y2_yavn[K - 1] = mu2(timing) / beta2;

            //if (k_yavn == 3)
            output_yavn(timing);

            k_yavn++;
        }
    }
}