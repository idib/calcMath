import cMath.Matrix;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by idib on 08.02.17.
 */
public class lab3 {
    public static void main(String[] args) throws Exception {
        double eps = (double) 1e9;
        NumberFormat formater = new DecimalFormat("0.#####E0##");

        Matrix A, B;

        // test1
        A = new Matrix("src/tests/matrix/matrix_a_5");
        B = new Matrix("src/tests/matrix/matrix_b_5");

        //test2
//        A = new Matrix("src/tests/matrix/matrix_a_6");
//        B = new Matrix("src/tests/matrix/matrix_b_6");

        //test3
//        A = new Matrix("src/tests/matrix/matrix_a_7");
//        B = new Matrix("src/tests/matrix/matrix_b_7");

        System.out.println("eps");
        System.out.println(formater.format(eps));
        System.out.println();

        System.out.println("A=");
        System.out.println(A.toString());
        System.out.println();


        System.out.println("B=");
        System.out.println(B.toString());
        System.out.println();


        double det = A.det();
        System.out.println("det(A)=");
        System.out.println(det);
        System.out.println();

        try {
            if (det != 0) {
                System.out.println("Jacobi metod");
                System.out.println("count iterations:");

                Matrix X = A.Jacobi(B, eps);
                Matrix AX = Matrix.mul(A, X);
                Matrix BAX = Matrix.sub(B, AX);

                System.out.println(X.countIterations);
                System.out.println();

                System.out.println("X=");
                System.out.println(X);
                System.out.println();

                System.out.println("B - AX =");
                System.out.println(BAX);


                System.out.println("Seidel metod");
                System.out.println("count iterations:");

                Matrix X1 = A.Seidel(B, eps);
                Matrix AX1 = Matrix.mul(A, X1);
                Matrix BAX1 = Matrix.sub(B, AX1);


                System.out.println(X1.countIterations);
                System.out.println();

                System.out.println("X=");
                System.out.println(X1);
                System.out.println();

                System.out.println("B - AX =");
                System.out.println(BAX);


                System.out.println();
                System.out.println("Different count iterations");
                System.out.println(Math.abs(X.countIterations - X1.countIterations));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
